
const express = require('express');
var cors = require('cors');
const bodyParser = require("body-parser")
const routesLogin = require('./routes/login');
const routesAlquiler = require('./routes/alquiler');
const routesRequistro = require('./routes/registro');
const routesIngresoTarjeta = require('./routes/ingreso-tarjeta');
const routesRecord = require('./routes/record');

const PORT = process.env.PORT || 1337;

const app = express();

app.use(cors())
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

app.use((req, res, next) => {
  res.set('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.use(routesLogin);  
app.use(routesAlquiler);  
app.use(routesRequistro);  
app.use(require('./routes/pago'));
app.use(routesIngresoTarjeta);
app.use(routesRecord);

var server = app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

module.exports = server;