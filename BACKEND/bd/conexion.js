const mysql = require('mysql');
require('dotenv').config();

const connection = mysql.createConnection({
  user: process.env.DB_USER|| 'admin', // 'db-user'
  password: process.env.DB_PASS|| '123456789', // 'db-password'
  database: process.env.DB_NAME|| 'ayd1', // 'database'
  host: process.env.DB_IP || 'ayd1.c6ywdk1yuqmq.us-east-2.rds.amazonaws.com', // 'ip'
  port: process.env.DB_PORT || 3306, // 'port'
});

module.exports = connection;