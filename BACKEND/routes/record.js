const express = require('express');
const connection = require('../bd/conexion');
const router = express.Router();

connection.connect(err => {
    if (err) {
        console.log(err.message);
        return;
    }
    console.log('Conexion a BD AWS exitosa');
});

router.get('/record/:idUser', (req, res) => {
    const { idUser } = req.params;
    connection.query(`SELECT name, image, monto FROM Alquiler
    INNER JOIN Movie M on Alquiler.idMovie = M.idMovie 
    INNER JOIN Pago P on Alquiler.idAlquiler = P.idAlquiler
    WHERE idUsuario = ${idUser};`, (err, rows) => {
        try {
            if (err) {
                res.json({status: 400, data: "", message: "Error in DataBase"})
            }
    
            let dataObject = rows;
            res.json({status: 200, data: dataObject, message: "Successfully"});
        } catch (err) {
            console.log(err);
        }

    });
});

module.exports = router;