const express = require("express")
const DataBase = require("../bd/conexion")
const router = express.Router();


router.post('/registro',(request,response) =>{

    let query = "INSERT INTO Usuario SET ?";
    var usuarioObject = {
        usuario:request.body.usuario,
        correo:request.body.correo,
        contrasena:request.body.contrasena,
        nombres:request.body.nombres,
        apellidos:request.body.apellidos,
        DPI:request.body.DPI,
        edad:request.body.edad,
        transacciones:0
    }

    DataBase.query(query,usuarioObject,(err,result)=>{

        if (err){       
            console.log(err)
        
            response.status(400).json({
                data:err,
                status:0
            })
        }else{
                        
            response.status(200).json({
                data:result,
                status: 1
            })
        }
    });

});


module.exports = router