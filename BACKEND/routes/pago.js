const express = require('express');
const connection = require('../bd/conexion');
const router = express.Router();

var idAlquiler;

const getId = (queryPago) => {

    //Insertar un Pago
    connection.query(queryPago, (err, result) => {
        try {
            console.log(err);
            if (err) {
                console.log(err);
                return;
            } else {
                //Successfully
            }

        } catch (error) {
            console.error(error);
            return;
        }
    });
}

router.get('/tarjetas/:id', (req, res) => {

    const idUser = req.params.id;

    const query = `SELECT * FROM Tarjeta WHERE idUsuario = ${idUser}`;

    connection.query(query, (err, rows) => {
        try {
            if (err) {
                res.send({ status: 400, data: "", message: "Error in data base" });
                return;
            }

            if (rows.length !== 0) {
                res.send({ status: 200, data: rows, message: "Get cards successfully" });
            } else {
                res.send({ status: 200, data: [], message: "Don't have cards" });
            }
        } catch (error) {
            console.error(error);
        }
    });
});


router.post('/pago', (req, res) => {

    const { nombreUsuario, movies, idUsuario, idTarjeta, total } = req.body;
    let codigo;
    let monto;

    for (let i = 0; i < movies.length; i++) {
        //item.idMovie,
        //item.precio,
        //item.chargeRate

        //Número random para la llave de alquiler
        codigo = Math.floor(Math.random() * (99999999 - 10000000)) + 10000000;

        //Calcular total
        setTimeout(() => {
            monto = movies[i].precio * movies[i].chargeRate;
            inserAlquiler(monto, codigo, movies[i].idMovie, idUsuario, nombreUsuario, idTarjeta);
        }, 1000);
    }

    let queryTarjeta = `UPDATE Tarjeta SET montoEnCuenta = montoEnCuenta - ${total} where idTarjeta = ${idTarjeta};`;
    
    connection.query(queryTarjeta, (err, result) => {
        try {
            
            if (err) {
                res.send({ status: 400, message: "Error en la base de datos, no se pudo acutalizar el monto" });
                return    
            } else {
                //Successfully
            }

        } catch (error) {
            console.log(error);
            res.send({ status: 400, message: "Error en la base de datos, no se pudo acutalizar el monto" });
            return
        }
    });

    res.send({status: 200, message: "Pago completado satisfactoriamente"});
});

const inserAlquiler = (monto, codigo, idMovie, idUsuario, nombreUsuario, idTarjeta) => {

    //Insertar un alquiler
    queryAlquiler = `INSERT INTO Alquiler (code, idMovie, idUsuario) values ('${codigo}', ${idMovie}, ${idUsuario});`;

    connection.query(queryAlquiler, (err, result) => {
        try {

            if (err) {
                //res.send({ status: 400, message: "Error en la base de datos, no se pudo conectar e insertar el alquiler" });
                return undefined;
            } else {

                //Función para inesretar el pago
                queryPago = `INSERT INTO Pago (codigoVerificar, monto, moneda, idAlquiler, idTarjeta)
                values ('${nombreUsuario}', ${monto}, 'Quetzal', ${result.insertId}, ${idTarjeta});`;
                getId(queryPago);
            }

        } catch (error) {
            console.error(error);
            //res.send({ status: 400, message: "Error en la base de datos, no se pudo conectar e insertar el alquiler" });
            return undefined;
        }
    });
}

module.exports = router;