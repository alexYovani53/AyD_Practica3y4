const express = require('express');
const connection = require('../bd/conexion');
const router = express.Router();


router.get('/alquiler', (req, res) => {


    connection.query(`select distinct m.*, l.*  from Movie m
    inner join MovieLenguage ml ON ml.idMovie = m.idMovie
    inner join Lenguage l on l.idLenguage = ml.idLenguage
    where active = 1;`, (err, rows) => {
  
        if (err) {
            res.json({status: 400, data: "", message: "Error in DataBase"})
        }else{
            if (rows.length === 0){
                res.json({status: 200, data: [], message: `No encontro peliculas disponibles`});
            } else {
                res.json({status: 200, data: rows, message: "Se encontro peliculas"});
            }
        }
    });
});


module.exports = router;