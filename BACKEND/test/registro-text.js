process.env.NODE_ENV = 'test';

var chai = require('chai');
let chaiHttp = require('chai-http');
var server = require("../index");
var should = chai.should();

chai.use(chaiHttp);

describe('/POST/registroUsuario', () => {
    it('Completar registro de usuario', (done) => {
    chai.request(server)
    .post(`/registro`)
    .send({
        usuario:"yovani53",
        correo:"alexyovani53@gmail.com",
        contrasena:"123456",
        nombres:"Alex Y.",
        apellidos:"Yovani J.",
        DPI:"287281124",
        edad:"23",
        transacciones:0
    })
    .end((err, res) => {
        res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('status');
        done();
        });
    });
});

// describe('/POST/registroUsuario/fail', () => {
//     it('Completar registro de usuario fail', (done) => {
//     chai.request(server)
//     .post(`/registro`)
//     .send({
//         usuario:"yovani53",
//         correo:"alexyovani53@gmail.com",
//         contrasena:"123456",
//         nombres:"Alex.",
//         apellidos:"Yovani J.",
//         DPI:"a", //va una letra
//         //edad:"23",
//         transacciones:0
//     })
//     .end((err, res) => {
//         res.should.have.status(200);
//             res.body.should.be.a('object');
//             res.body.should.have.property('status');
//         done();
//         });
//     });
// });
