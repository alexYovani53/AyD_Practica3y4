process.env.NODE_ENV = 'test';

var chai = require('chai');
let chaiHttp = require('chai-http');
var server = require("../index");
var should = chai.should();

chai.use(chaiHttp);

describe('/POST/ingreso-tarjeta', () => {
    it('Registrar una tarjeta', (done) => {
    chai.request(server)
    .post(`/ingreso-tarjeta`)
    .send({
        numero:"7894561230000",
        nombre:"Nombre DesdeTest",
        fechaExp:"02/22",
        usuario:6,
        montoEnCuenta:0
    })
    .end((err, res) => {
        res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('status');
        done();
        });
    });
});

// describe('/POST/ingreso-tarjeta/fail', () => {
//     it('Registrar una tarjeta fail', (done) => {
//     chai.request(server)
//     .post(`/ingreso-tarjeta`)
//     .send({
//         numero:"7894561230000",
//         nombre:"Nombre DesdeTest",
//         fechaExp:"02/22",
//         usuario:100, //no existe el usuario
//         montoEnCuenta:0
//     })
//     .end((err, res) => {
//         res.should.have.status(200);
//             res.body.should.be.a('object');
//             res.body.should.have.property('status');
//         done();
//         });
//     });
// });