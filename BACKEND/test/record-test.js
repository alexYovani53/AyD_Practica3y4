process.env.NODE_ENV = 'test';

var chai = require('chai');
let chaiHttp = require('chai-http');
var server = require("../index");
var should = chai.should();

chai.use(chaiHttp);

describe('/GET/record', () => {
    it('Debería existir el usuario con las peliculas', (done) => {
    chai.request(server)
    .get(`/record/1`)
    .end((err, res) => {
        res.should.have.status(200);
            res.body.should.have.property('status');
            res.body.should.have.property('message').eql("Successfully");
        done();
        });
    });
});

// describe('/GET/record/fail', () => {
//     it('No existe el usuario', (done) => {
//     chai.request(server)
//     .get(`/record/1000`)
//     .end((err, res) => {
//         res.should.have.status(200);
//             res.body.should.have.property('status');
//             res.body.should.have.property('message').eql("Successfully");
//         done();
//         });
//     });
// });