process.env.NODE_ENV = 'test';

var chai = require('chai');
let chaiHttp = require('chai-http');
var server = require("../index");
var should = chai.should();

chai.use(chaiHttp);

describe('/GET/alquiler', () => {
    it('Muestra las pelis a alquilar', (done) => {
    chai.request(server)
    .get(`/alquiler`)
    .send()
    .end((err, res) => {
        res.should.have.status(200);
        //res.body.should.be.a('array');
        res.body.should.have.property('status');
        res.body.should.have.property('message').eql("Se encontro peliculas");
        done();
        });
    });
});

// describe('/GET/alquiler/fail', () => {
//     it('Muestra las pelis a alquilar fail', (done) => {
//     chai.request(server)
//     .get(`/alquiler/1`) //esta no es la ruta
//     .send()
//     .end((err, res) => {
//         res.should.have.status(200);
//         //res.body.should.be.a('array');
//         res.body.should.have.property('status');
//         res.body.should.have.property('message').eql("Se encontro peliculas");
//         done();
//         });
//     });
// });