process.env.NODE_ENV = 'test';

var chai = require('chai');
let chaiHttp = require('chai-http');
var server = require("../index");
var should = chai.should();

chai.use(chaiHttp);

describe('/POST/login', () => {
    it('It should return Successfully', (done) => {
    chai.request(server)
    .post(`/login`)
    .send({user_name: "daniel123", password : "1234"})
    .end((err, res) => {
        res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('status');
            res.body.should.have.property('message').eql("Successfully");
        done();
        });
    });
});

// describe('/POST/login/fail', () => {
//     it('It should return fail', (done) => {
//     chai.request(server)
//     .post(`/login`)
//     .send({user_name: "alpaca", password : "aplaca2"}) //no existe ese usuario
//     .end((err, res) => {
//         res.should.have.status(200);
//             res.body.should.be.a('object');
//             res.body.should.have.property('status');
//             res.body.should.have.property('message').eql("Successfully");
//         done();
//         });
//     });
// });
