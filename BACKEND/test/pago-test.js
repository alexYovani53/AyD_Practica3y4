process.env.NODE_ENV = 'test';

var chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();

chai.use(chaiHttp);

describe('/GET/tarjetas', () => {
    it("The cards return successfully", (done) => {
        chai.request(server)
        .get('/tarjetas/7')
        .end( (err, res) => {
            res.should.have.status(200);
            res.should.have.be.a('object');
            res.body.should.have.property('status');
        done();
        });
    });
});

describe('POST/pago', () => {
    it("The Pay return Successfully", (done) => {
        chai.request(server)
        .post('/pago')
        .send({
            nombreUsuario: 'elian_estrada',
            movies: [{"idMovie": 1, "precio": 100, "chargeRate": 5}],
            idUsuario: 7,
            idTarjeta: 10,
            total: 500
        })
        .end((err, res) => {
            res.should.have.status(200);
            res.should.have.be.a('object');
            res.body.should.have.property('status');
        done();
        });
    });
});


// describe('/GET/tarjetas/fail', () => {
//     it("The cards return fail", (done) => {
//         chai.request(server)
//         .get('/tarjetas/10000') //no existe el usuario
//         .end( (err, res) => {
//             res.should.have.status(200);
//             res.should.have.be.a('object');
//             res.body.should.have.property('status')
//             res.body.should.have.property('message').eql('Get cards successfully');
//         done();
//         });
//     });
// });

// describe('POST/pago/fail', () => {
//     it("The Pay return fail", (done) => {
//         chai.request(server)
//         .post('/pago')
//         .send({
//             nombreUsuario: 'elian_estrada',
//             movies: [{"idMovie": "12", "precio": 100, "chargeRate": 5}],
//             idUsuario: "7", 
//             idTarjeta: "100",
//             total: 500
//         })
//         .end((err, res) => {
//             res.should.have.status(200);
//             res.should.have.be.a('object');
//             res.body.should.have.property('status');
//             res.body.should.haee.property('message').eql('Successfull');
//         done();
//         });
//     });
// });