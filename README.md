---
tags: Ayd1, USAC, 1S2022, Practica3y4
---

# Practica 3 y 4
> ### Integrantes
> |No.|Carné| Nombre |
> |---|---|---|
> |1|201602912| Alex Yovani Jerónimo Tomás|
> |2|201800722| José Daniel Velásquez Orozco |
> |3|201314808| Leslie Fabiola Morales González|
> |4|201503906| Gerson Gabriel Reyes Melgar|
> |5|201806838| Elian Saúl Estrada Urbina|


## Planificación

<details><summary>Planificación(+)</summary>

<p>
    
Se realizaron reuniones virtuales por medio de google meet, en las cuales se utilizó el proceso SCRUM, siguiendo los pasos planning, implementation, review y retrospect. Partiendo del sprint backlog y llegando a la definición de lo que fue completado. 
    
Las reuniones se encuentran en el siguiente enlace: [Reuniones](https://drive.google.com/drive/folders/1liC52_LGLXCAkWQ5dOln00NB_zzdqN5q)
    
![](https://i.imgur.com/1GIp6ER.png)

Los requisitos sobre los cuales trabajamos se desglosan de la siguiente manera:    
    
Funcionales:
    
ACCESO
* Para registrarse los datos a ingresar son Usuario, Nombre Completo, Contraseña e imagien de foto de perfil.
* Para registrarse en la página, el usuario debe confirmar su contraseña al ingresarla dos veces y el sistema verificará que sea correcta.
* Para que un usuario pueda acceder, debe ingresar su nombre de usuario y contraseña. 
    
ALQUILER
* El usuario tendrá acceso a un catálogo de películas disponibles para poder rentar. 
* Las películas podrán ser añadidas a un carrito de compras, el cual podrá verificarse una vez la selección haya sido completada.
    
REGISTRO METODO DE PAGO
* El usuario tendrá la posibilidad de agregar los datos de su método de pago preferido para completar el pago del alquiler del servicio.
* Un formulario será desplegado, para que el usuario pueda ingresar dichos datos.
    
PAGO
* El usuario podrá seleccionar su método de pago previamente ingresado para que se realice el cobro por el monto en su carito de compras. 
    
No funcionales:
    
* Los permisos de acceso solamente podrán ser manejados por el administrador.
* Unicamente el usuario que es dueño de un perfil podrá acceder a los datos de pago que registró 
* El usuario podra acceder a las peliculas adquiridas unicamente desde su perfil, el cual deberá coicidir con el perfil en el que el pago fue realizado.
* Unicamente el propietario de la cuenta podrá observar los datos de su perfil y no tendrá acceso a las contraseñas del resto de los usuarios. 

</p>

</details>

## Codificación

<details><summary>Codificación(+)</summary>
<p>
    
Para la codificación se trabajo con Node.js y el framework para aplicaciones web, Angular.
    
![](https://i.imgur.com/gFBHQS2.png)
    
Por otra parte, para el versionamiento se utilizó un repositorio en gitlab, en el cual se trabajó un arbol de versionamiento con diferentes ramas, asignadas de acuerdo a los modulos y features a lanzar. Dicho repositorio se encuentra en la siguiente dirección: 
    
[Repositorio Grupo 1](https://gitlab.com/alexYovani53/AyD_Practica3y4.git)
    
![](https://i.imgur.com/dBL8FBl.png)


    
Las ramas utilizadas se desglozan de la siguiente manera: 

![](https://i.imgur.com/ImmjtW9.png)

    
</p>
    
 </details>

## Compilación

<details><summary>Compilación(+)</summary>
<p>
    
Para el entorno de la aplicación durante el desarrollo trabajamos de modo local. El frontend fue trabajado en el puerto 4200 y el backend en el puerto 1337. El manejo de la base de datos fue realizado por medio de MySQL y AWS. 
    
![](https://i.imgur.com/x9nyG07.png)

    
</p>
</details>
 
## Prueba

<details><summary>Prueba(+)</summary>
Para las pruebas se utilizó Chai y C8 de code coverage. De esta manera nos fue posible realizar pruebas unitarias sobre los procesos establecidos en backend. 
    
![](https://i.imgur.com/bKWSNi8.png)
    

### Test Unit para algunas fuciones 
![](https://i.imgur.com/znvdjVr.png)



</details>


## Puesta en marcha

<details><summary>Puesta en marcha(+)</summary>
<p>
        
El seguimiento y la gestión de modulos se realizó por medio de un tablero en la herramienta AzureDevOps del cual se adjunta una visualización a continuación: 
    
![](https://i.imgur.com/BSGjKCD.png)

Se utilizo el tablero de azure con sus respectivas asignaciones 
    
![](https://i.imgur.com/mU4RoyR.jpg)

    
</p>  
</details>
 
 
## Funcionamiento

<details><summary>Funcionamiento(+)</summary>
<p>
    
Pruebas Unitarias
 
### Post Login

Prueba realizada sobre el método que permite verificar que el usuario y la contraseña son validos y coinciden para poder ingresar al perfil en cuestión. 

![](https://i.imgur.com/UL3lpIM.png)

    
### Post ingreso-tarjeta

Prueba realizada para la función que registra los datos de la tarjeta en la base de datos
    
![](https://i.imgur.com/O9JJZzJ.png)

    
### Post registro

Prueba realizada para la función que registra los datos del usuario dentro de la aplicación
    
![](https://i.imgur.com/eGfcpF1.png)

### Get Tarjetas
    
Prueba realizada para la función que permite obtener las tarjetas de un usuario específico:
    
![](https://i.imgur.com/mNSlXTZ.png)
 
### Post Pago
    
Prueba realizada para la función que permite realizar el pago del alquiler de películas
    
![](https://i.imgur.com/2vFu5SH.png)
    
### Get alquilar

Prueba realizada para la función que devuelve las posibles películas para alquilar. 
    
![](https://i.imgur.com/7Lw4YJj.png)

    
</p>
</details>
 
  
## Supervisión

<details><summary>Supervisión(+)</summary>
<p>
    
Una vez que cada integrante completó sus tareas asignadas, y se realizaron las pruebas unitarias, se trabajaron los bugfix para realizar cualquier arreglo necesario que permitiera el funcionamiento correcto de la aplicación.
    
![](https://i.imgur.com/kLMtb6K.png)

![](https://i.imgur.com/7oUSi51.png)

    
</p>
</details>


## CI CD

<details><summary>Integración Continua(+)</summary>
<p>
    
Se utilizo CICD de Gitlab en el cual se crearon los respectivos pipelines haciendo uso de docker y
archivo de configuración .yaml
    
![](https://i.imgur.com/oBuEk2E.jpg)
    
Mensajes de información al correo
    
![](https://i.imgur.com/SKEUMwm.jpg)

</p>
</details>