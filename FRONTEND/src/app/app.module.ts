import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NavigComponent } from './navig/navig.component';
import { PaginaInicioComponent } from './pagina-inicio/pagina-inicio.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { PayComponent } from './pay/pay.component';
import { IngresoTarjetaComponent } from './ingreso-tarjeta/ingreso-tarjeta.component';
import { SaldoComponent } from './saldo/saldo.component';
import { RecordComponent } from './record/record.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigComponent,
    PaginaInicioComponent,
    LoginComponent,
    RegistroComponent,
    PayComponent,
    IngresoTarjetaComponent,
    SaldoComponent,
    RecordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
