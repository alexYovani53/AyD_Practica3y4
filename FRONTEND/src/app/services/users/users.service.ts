import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from "rxjs";
import {API_URL} from "../URL";

@Injectable({
  providedIn: "root"
})
export class UsersService {
  constructor(private http: HttpClient) {}

  login(user: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'key': 'x-api-key',
        'value': 'NNctr6Tjrw9794gFXf3fi6zWBZ78j6Gv3UCb3y0x',
      })
    }
    return this.http.post(`${API_URL}/login`,
      JSON.stringify(user),
      {headers: httpOptions.headers});
  }

  updatePerfil(usuario: any, idusuario: number): Observable<any>
  {
    return this.http.put(`${API_URL}/updatePerfil/${idusuario}`,usuario);
  }

  getPeliculas(): Observable<any>
  {
    return this.http.get(`${API_URL}/alquiler`);
  }

  getRecord(idUser: any): Observable<any>
  {
    return this.http.get(`${API_URL}/record/${idUser}`);
  }
}
