import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaInicioComponent } from './pagina-inicio/pagina-inicio.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from "./registro/registro.component";
import { PayComponent } from './pay/pay.component';
import { IngresoTarjetaComponent } from './ingreso-tarjeta/ingreso-tarjeta.component';
import { SaldoComponent } from './saldo/saldo.component';
import {RecordComponent} from "./record/record.component";

const routes: Routes = [
  {
    path: 'Login',
    component:LoginComponent
  },
  {
    path: 'Registro',
    component:RegistroComponent
  },
  {
    path : 'paginaInicio',
    component:PaginaInicioComponent
  },
  {
    path: 'Pago',
    component:PayComponent
  },
  {
    path : 'ingresoTarjeta',
    component:IngresoTarjetaComponent
  },
  {
    path : 'saldo',
    component:SaldoComponent
  },
  {
    path: 'record',
    component: RecordComponent
  },
  {
    path : '**', redirectTo: 'Login'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
