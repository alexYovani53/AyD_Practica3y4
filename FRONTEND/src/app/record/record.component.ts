import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UsersService} from "../services/users/users.service";

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {

  constructor(private router: Router, public userService: UsersService) { }

  idUser: string | null = "";
  movies: any = [];

  ngOnInit(): void {
    this.idUser = localStorage.getItem("idusuario");

    // console.log(this.idUser);

    this.userService.getRecord(this.idUser).subscribe(res => {
      // console.log(res);

      if (res.status != 200) {
        console.error(res.message);
        alert(`Error: ${res.message}`);
        return;
      }

      res.data.forEach((element: any) => {
        element.date = this.randomDate();
      });

      this.movies = res.data;
      console.log(this.movies);
    }, err => {
      console.error(err);
      alert("Datos erróneos");
    });
  }

  randomDate(): string {
    return  new Date(new Date('2019-02-12T01:57:45.271Z').getTime() + Math.random() * (new Date('2022-02-12T01:57:45.271Z').getTime() - new Date('2019-02-12T01:57:45.271Z').getTime())).toDateString();
  }
}
