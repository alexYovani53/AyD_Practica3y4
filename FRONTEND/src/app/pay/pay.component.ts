import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';

import { API_URL } from '../services/URL';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  storageCarrito = localStorage.getItem('carrito');
  storageUser = localStorage.getItem('username');
  host = API_URL;
  flag = false;

  userName: string;
  card: any;
  monto: any;
  movies: any[];
  cards: any;
  acum: number;

  constructor(private router: Router) {
    this.userName = '';
    this.card = '';
    this.movies = this.storageCarrito !== null ? JSON.parse(this.storageCarrito) : [];
    this.acum = 0;
  }

  ngOnInit(): void {
    this.movies.forEach(item => {
      this.acum += (item.precio * item.chargeRate);
    });
    this.getCards();
  }

  async getCards() {
    let response = await axios.get(`${this.host}/tarjetas/${localStorage.getItem('idusuario')?.toString()}`);

    this.cards = response.data.data;
  }

  execute() {
    if (this.userName === this.storageUser) {
      this.pay();
    } else {
      alert("El nombre de usuario no conincide");
    }
  }

  async pay() {
    console.log("Pago completado");

    this.card = JSON.parse(this.card);

    if (this.card.monto >= this.acum) {
      const response = await axios.post(`${this.host}/pago`, {
        nombreUsuario: this.userName,
        movies: this.movies,
        idUsuario: localStorage.getItem('idusuario'),
        idTarjeta: this.card.id,
        total: this.acum
      });

      console.log("response", response);
      
      if (response.data.status === 200){
        alert("Pago completado");
        this.router.navigate(['/paginaInicio']);
      } else {
        alert("Error al momento de realizar el pago");
      }

    } else {
      alert("La tarjeta no cuenta con fondos suficientes para el pago :'/");
    }
  }

  onChange(value: any) {

    if (value.target.value == 0) {
      console.log("Registrar tarjeta");
      this.router.navigate(['ingresoTarjeta']);
    }
  }

  getInfo(id: string, monto: number) {
    return `{"id": ${id}, "monto": ${monto}}`;
  }

}
