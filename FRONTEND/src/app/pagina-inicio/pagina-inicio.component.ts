import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagina-inicio',
  templateUrl: './pagina-inicio.component.html',
  styleUrls: ['./pagina-inicio.component.css']
})
export class PaginaInicioComponent implements OnInit {

  idiomas:any = [];
  alquiler: any = [];

  constructor(private userService: UsersService, private router: Router) {
   }

  ngOnInit(): void {
    this.alquiler = []
    this.idiomas = []
    localStorage.setItem('carrito', this.alquiler);
    this.construir();
  }

  construir(){
    this.userService.getPeliculas().subscribe(

      res => {

        res.data.forEach((element: any) => {
          if(!this.buscarIdioma(element.description + "-" + element.code)){
            this.idiomas.push({idioma: element.description + "-" + element.code, peliculas: []});
          }
        });
       

        res.data.forEach((element: any) => {
          this.idiomas.forEach((elementIdioma: any) => {
            if(element.description + "-" + element.code == elementIdioma.idioma){
              elementIdioma.peliculas.push(element);
            }
          });
        });
        //console.log(this.idiomas[0])
      },
      err => {
        console.error(err);
        alert("Ocurrio un problema al obtener las pelis.");
      }
    )
  }

  buscarIdioma(idioma : string){

    for (let x=0; x < this.idiomas.length; x++){
      if(this.idiomas[x].idioma == idioma){
        return true;
      }
    }
    return false;
  }

  changeCheckbox(peliSeleccionada: any) {
    let entro = false
    let i = 0

    for(let x=0; x < this.alquiler.length; x++){
      if(this.alquiler[x].idMovie == peliSeleccionada.idMovie && this.alquiler[x].code == peliSeleccionada.code){
        entro = true
        i = x
        break
      }
    }
   // console.log("valor entro: ", entro);
    if(!entro){
      this.alquiler.push(peliSeleccionada) //ingresarla al carrito (seleccionada)
    }else{
      //console.log("pos ",i);
      this.alquiler.splice(i, 1);// sacarla del carrtio (deseleccionada)
    }
    console.log("lista alquiler: ");
    console.log(this.alquiler);
  }

  finalizar(opcion : number, peli : any){
    if(opcion == 1){
      this.alquiler = []
      this.alquiler.push(peli)
    }
    for(let x=0; x < this.alquiler.length; x++){
      this.alquiler[x].image = ""
    }
    localStorage.setItem('carrito', JSON.stringify(this.alquiler));
    console.log(this.alquiler)
    this.router.navigate(['/Pago']);
  }

}

