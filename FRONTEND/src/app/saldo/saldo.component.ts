import { Component, OnInit } from '@angular/core';
import axios from 'axios';

import { API_URL } from '../services/URL';

@Component({
  selector: 'app-saldo',
  templateUrl: './saldo.component.html',
  styleUrls: ['./saldo.component.css']
})
export class SaldoComponent implements OnInit {

  idUser:any;
  cards:any;
  flag: boolean;

  constructor() {
    this.idUser =  localStorage.getItem('idusuario');
    this.flag = false;
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    this.flag = false;
    const response = await axios.get(`${API_URL}/tarjetas/${this.idUser}`);
    console.log(response);
    

    if (response.data.status === 200){
      this.cards = response.data.data;

      if (this.cards.length !== 0){
        this.flag = true;
      } 

    } else {
      alert("Error al obtener las tarjetas");
      this.flag = false;
    }
  }
}
