import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import axios from 'axios';
import { API_URL } from '../services/URL';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  crearUsuario: FormGroup;
  ErrorContrasena: String = "";
  ErrorFoto: String = "";
  ErrorNombres: String = "";
  ErrorUsuario: String = "";
  ErrorApellidos: String = "";
  ErrorCorreo: String = "";
  ErrorDPI: String = "";
  ErrorEdad: String = "";

  foto:String="";
  nombreFoto:String = "";

  constructor(private formulario: FormBuilder,private router:Router) { 
    this.crearUsuario = this.formulario.group({
      nombres:['',Validators.required],
      apellidos:['',Validators.required],
      correo:['',Validators.required],
      usuario:['',Validators.required],
      contrasena:['',Validators.required],
      confirmar:['',Validators.required],
      edad:['',Validators.required],
      DPI:['',Validators.required],
      fototext:[''],
      foto:['']
    })
  }

  ngOnInit(): void {
  }

  test(){
    console.log(this.crearUsuario);
    this.ErrorContrasena = "";
    this.ErrorFoto="";
    this.ErrorNombres="";
    this.ErrorUsuario ="";
    this.ErrorApellidos = "";
    this.ErrorDPI = "";
    this.ErrorEdad = "";

    if(!this.crearUsuario.valid){
      if(this.crearUsuario.value.contrasena != this.crearUsuario.value.confirmar){
        this.ErrorContrasena="Las contraseñas no coinciden";
      }

      if(this.crearUsuario.value.usuario =="") this.ErrorUsuario = "Ingrese un usuario";
      if(this.crearUsuario.value.nombres =="") this.ErrorNombres = "Ingrese sus nombres";      
      if(this.crearUsuario.value.foto =="") this.ErrorFoto = "Seleccione una foto";
      if(this.crearUsuario.value.contrasena =="") this.ErrorContrasena = "Ingrese su contraseña";
      if(this.crearUsuario.value.apellidos =="") this.ErrorApellidos = "Ingrese sus apellidos";
      if(this.crearUsuario.value.correo =="") this.ErrorCorreo = "Ingrese un correo";
      if(this.crearUsuario.value.edad =="") this.ErrorEdad = "Ingrese su edad";
      if(this.crearUsuario.value.DPI =="") this.ErrorDPI = "Ingrese su nÚmero de DPI";
      

      return;
    }
    axios.post(`${API_URL}/registro`,{
      nombres:this.crearUsuario.value.nombres,
      contrasena:this.crearUsuario.value.contrasena,
      usuario:this.crearUsuario.value.usuario,
      correo:this.crearUsuario.value.correo,
      apellidos:this.crearUsuario.value.apellidos,
      DPI:this.crearUsuario.value.DPI,
      edad:this.crearUsuario.value.edad,
      foto:this.foto
    }).then(result=>{
      console.log(result)
        if(result.data.status ==1){

          this.router.navigate(['/Login']);
        }
      }
    ).catch(err=>{
      console.log(err)
    })

  }


  getBase64 = (file:any)=>{
    return new Promise(resolve=>{
        let baseURL;            
        let reader =  new FileReader();

        reader.readAsDataURL(file);

        reader.onload=()=>{
            baseURL =  reader.result;
            resolve(baseURL);
        }
        
    });

  }

  
  fileChange(fileInput:any){
    this.foto = "";
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      this.nombreFoto = fileInput.target.files[0].name;

      this.getBase64(fileInput.target.files[0])
      .then((resultado) =>{
          this.foto = String(resultado).split(",")[1];
      })
      .catch(err =>{
          console.log(err)
      });
     
    }

    
  }
}