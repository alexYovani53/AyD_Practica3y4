import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoTarjetaComponent } from './ingreso-tarjeta.component';

describe('IngresoTarjetaComponent', () => {
  let component: IngresoTarjetaComponent;
  let fixture: ComponentFixture<IngresoTarjetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngresoTarjetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
