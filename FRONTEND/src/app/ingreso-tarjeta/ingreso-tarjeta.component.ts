import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import axios from 'axios';
import { API_URL } from '../services/URL';

@Component({
  selector: 'app-ingreso-tarjeta',
  templateUrl: './ingreso-tarjeta.component.html',
  styleUrls: ['./ingreso-tarjeta.component.css']
})
export class IngresoTarjetaComponent implements OnInit {

  crearTarjeta: FormGroup;
  ErrorNumero: String = "";
  ErrorNombre: String = "";
  ErrorFechaExp: String = "";

  constructor(private formulario: FormBuilder,private router:Router) { 

    this.crearTarjeta = this.formulario.group({
      numero:['',Validators.required],
      nombre:['',Validators.required],
      fechaExp:['',Validators.required],
      usuario:0
    })

  }

  ngOnInit(): void {
  }

  test(){
    console.log(this.crearTarjeta);
    this.ErrorNumero = "";
    this.ErrorNombre="";
    this.ErrorFechaExp="";

    if(!this.crearTarjeta.valid){

      if(this.crearTarjeta.value.numero =="") this.ErrorNumero = "Ingrese numero";
      if(this.crearTarjeta.value.nombre =="") this.ErrorNombre = "Ingrese nombre";      
      if(this.crearTarjeta.value.fechaExp =="") this.ErrorFechaExp = "Seleccione una fecha de expiracion";
      
      return;
    }

    let idUser = localStorage.getItem('idusuario'); 

    axios.post(`${API_URL}/ingreso-tarjeta`,{
      numero:this.crearTarjeta.value.numero,
      nombre:this.crearTarjeta.value.nombre,
      fechaExp:this.crearTarjeta.value.fechaExp,
      usuario:idUser
    }).then(result=>{
      console.log(result)

        if(result.data.status ==1){

          let back = confirm("Desea ir al Carrito de compra?");
          console.log(back);
          
          if (back == true){
            this.router.navigate(['/Pago']);
          } else {
            this.router.navigate(['/paginaInicio']);
          }
        }
      }
    ).catch(err=>{
      console.log(err)
    })

  }

}
